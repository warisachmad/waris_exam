import { Injectable } from '@angular/core';

// import { Http } from '@angular/http';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class APIServiceService {

  data:object[];

  constructor(private http:Http) { this.http
    .get('https://jsonplaceholder.typicode.com/users')

    .map(result => result.json())
    .catch(error => Observable.throw(error.json().error) || "Server Error")

    //get
    .subscribe(result => this.data = result); }

  //Get
  getData(){
    
    // return this.http
    // .get('https://jsonplaceholder.typicode.com/users')

    // .map(result => result.json())
    // .catch(error => Observable.throw(error.json().error) || "Server Error")

    //get
    // .subscribe(result => this.data = result);
  }

  //Post
  addData(obj: Object){

    let body = JSON.stringify(obj);
    let headers = new Headers({ "Content-Type" : "application/json" });
    let options = new RequestOptions({ headers : headers });

    this.http.post('https://jsonplaceholder.typicode.com/users', body, options)
    .subscribe(result => console.log(result.json()));

  }

}
