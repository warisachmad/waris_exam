import { Component, OnInit } from '@angular/core';
import { APIServiceService } from '../APIservice/apiservice.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  data:object[];

  constructor(private api:APIServiceService) { }

  //Get
  ngOnInit() {
    // this.api.getData()
    // .subscribe(result => this.data = result);
  }

  deleteToDo(index){
    this.api.data.splice(index, 1);
  }

}
