import { Component, OnInit } from '@angular/core';
import { APIServiceService } from '../APIservice/apiservice.service';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {

  data:object[];

  constructor(private api:APIServiceService) { }

  ngOnInit() {
  }

  newName : string = "";
  newEmail : string = "";
  newAddress : string = "";
  newPhoneNumber : string = "";
  newCompanyName : string = "";

  //Post
  addToDo(){
    this.api.data.push( {
        "id": 1,
        "name": this.newName,
        "username": "",
        "email": this.newEmail,
        "address": {
            "street": this.newAddress,
            "suite": "",
            "city": "",
            "zipcode": "",
            "geo": {
                "lat": "",
                "lng": ""
            }
        },
        "phone": this.newPhoneNumber,
        "website": "",
        "company": {
            "name": this.newCompanyName,
            "catchPhrase": "",
            "bs": ""
        }
    } );

    alert("User Add Succesfully");
    
    this.newName = "";  
    this.newEmail = "";
    this.newAddress = "";
    this.newPhoneNumber = "";
    this.newCompanyName = "";

  }

}
